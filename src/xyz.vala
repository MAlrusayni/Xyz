/* xyz.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Math;

/**
 * xyz is a small geometry library, it provide the basic geometry object and
 * thier related methods to make it easy to deal with these objects.<<BR>>
 * xyz library is not intend for drawing, it only represent geometry objects.
 *
 */
[CCode (gir_namespace = "Xyz", gir_version = "0.1")]
namespace Xyz {

	/**
	 * This method is not tested yet and it has ben marked as experimental.
	 *
	 * @since 0.1
	 * @return the calculated value.
	 */
	[Version (since = "0.1", experimental = true)]
	public double lerp (double v0, double v1, double t) {
		/* TODO: need review from mathematical person */
		return (1 - t) * v0 + t * v1;
	}

	/**
	 * Convert radian angle to degree.
	 *
	 * @param radian angle you want to convert
	 * @return degree angle.
	 * @since 0.1
	 * @see to_radian
	 */
	[Version (since = "0.1")]
	public double to_degree (double radian) {
		return radian * (180.0 / PI);
	}

	/**
	 * Convert degree angle to radian.
	 *
	 * @param degree angle you want to convert
	 * @return radian angle.
	 * @since 0.1
	 * @see to_degree
	 */
	[Version (since = "0.1")]
	public double to_radian (double degree) {
		return degree * (PI / 180.0);
	}
}
