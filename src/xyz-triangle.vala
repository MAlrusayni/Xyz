/* xyz-triangle.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xyz {

	/**
	 * This class represent a triangle in a 2D space and has three points member {@link top}, {@link left} and {@link right}.
	 *
	 * Triangle is a one of the basic shapes in geometry, this class can represent all shapes of triangles.<<BR>>
	 * this figure show you how this class looks like:<<BR>>
	 * {{xyz_triangle_figure_1.svg}}
	 *
	 * @since 0.1
	 */
	[Version (since = "0.1")]
	public class Triangle : Object {
		/**
		 * This is the top point, or you can call it top vertex or edge.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point top { get; set construct; }
		/**
		 * This is the left point, or you can call it left vertex or edge.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point left { get; set construct; }
		/**
		 * This is the right point, or you can call it right vertex or edge.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point right { get; set construct; }

		/**
		 * Calculating the area of this {@link Xyz.Triangle}.
		 *
		 * @return The area of this triangle.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double area () {
			/* Heron's formula is used here */
			var a = left.distance (right);
			var b = right.distance (top);
			var c = top.distance (left);
			var s = (a + b + c) / 2.0;
			return Math.sqrt (s * (s - a) * (s - b) * (s - c));
		}

		/**
		 * Return the centoid/midpoint of this {@link Xyz.Triangle}
		 *
		 * @return The centroid point.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point centroid () {
			return new Point ((1.0 / 3.0) * (top.x + left.x + right.x), (1.0 / 3.0) * (top.y + left.y + right.y));
		}

		/**
		 * Calculating the internal angle for {@link top} point.
		 *
		 * @return Angle in radian
		 * @since 0.1
		 * @see Xyz.Point.angle
		 */
		[Version (since = "0.1")]
		public double top_angle () {
			return top.angle (right, left);
		}

		/**
		 * Calculating the internal angle for {@link left} point.
		 *
		 * @return Angle in radian
		 * @since 0.1
		 * @see Xyz.Point.angle
		 */
		[Version (since = "0.1")]
		public double left_angle () {
			return left.angle (top, right);
		}

		/**
		 * Calculating the internal angle for {@link right} point.
		 *
		 * @return Angle in radian
		 * @since 0.1
		 * @see Xyz.Point.angle
		 */
		[Version (since = "0.1")]
		public double right_angle () {
			return right.angle (left, top);
		}

		/**
		 * Create a new {@link Xyz.Triangle} object from given points.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Triangle (Point top, Point left, Point right) {
			this.top = new Point.from_point (top);
			this.left = new Point.from_point (left);
			this.right = new Point.from_point (right);
		}

		/**
		 * Create a new {@link Xyz.Triangle} object from another triangle.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Triangle.from_triangle (Triangle other) {
			this (other.top, other.left, other.right);
		}

		/* TODO: add is_right_angle () method */
		/* TODO: add is_equilateral () method */
		/* TODO: add is_isosceles () method */
		/* TODO: add is_scalene () method */
		/* TODO: add is_obtuse () method */
		/* TODO: add is_acute () method */
		/* TODO: add is_degenerate () method */
	}
}
