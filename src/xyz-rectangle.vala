/* xyz-rectangle.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xyz {

	/**
	 * This class represent a rectangle in a 2D space and has two points member {@link start} and {@link end}
	 *
	 * this figure show you how this class looks like:<<BR>>
	 * {{xyz_rectangle_figure_1.svg}}
	 *
	 * @since 0.1
	 */
	[Version (since = "0.1")]
	public class Rectangle : Object {
		/**
		 * the left top point of this rectangle.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point start { get; set construct; }
		/**
		 * the bottom right point of this rectangle
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point end { get; set construct; }

		/**
		 * Calculating the area of this {@link Xyz.Rectangle}.
		 *
		 * @return area of this rectangle
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double area () {
			var left_bot_point = new Point (start.x, end.y);
			return left_bot_point.distance (end) * left_bot_point.distance (start);
		}

		/**
		 * Return the width of this {@link Xyz.Rectangle}.
		 *
		 * @return width of this rectangle.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double width () {
			return end.x - start.x;
		}

		/**
		 * Return the high of this {@link Xyz.Rectangle}.
		 *
		 * @return high of this rectangle.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double high () {
			return end.y - start.y;
		}

		/**
		 * Check if this {@link Xyz.Rectangle} representing a square or not.
		 *
		 * @return true if this rectangle represent a square or false if not.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public bool is_square () {
			return (width () == high ());
		}

		/**
		 * Return the diagonals for this {@link Xyz.Rectangle}.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double diagonals () {
			return start.distance (end);
		}

		/**
		 * Return the centoid/midpoint of this {@link Xyz.Rectangle}
		 *
		 * @return The centroid point.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point centroid () {
			return start.centroid (end);
		}

		/**
		 * Create a new {@link Xyz.Rectangle} object from given values.
		 *
		 * @param start_x the x for {@link start} point.
		 * @param start_y the y for {@link start} point.
		 * @param start_x the x for {@link end} point.
		 * @param start_y the y for {@link end} point.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Rectangle (double start_x, double start_y, double end_x, double end_y) {
			this.start = new Point (start_x, start_y);
			this.end = new Point (end_x, end_y);
		}

		/**
		 * Create a new {@link Xyz.Rectangle} object from given points.
		 *
		 * @param start the {@link start} point.
		 * @param end the {@link end} point
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Rectangle.from_points (Point start, Point end) {
			this (start.x, start.y, end.x, end.y);
		}

		/**
		 * Create a new {@link Xyz.Rectangle} object from given object and values.
		 *
		 * @param postion the {@link start} point.
		 * @param width the width for this rectangle.
		 * @param high the high for this rectangle.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Rectangle.from_wdith_high (Point postion, double width, double high) {
			var end = new Point (postion.x + width, postion.y + high);
			this.from_points (postion, end);
		}
	}
}
