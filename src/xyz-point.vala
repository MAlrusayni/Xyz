/* xyz-point.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Math;

namespace Xyz {

	/**
	 * This class represent a point in a 2D space and has two members {@link x}
	 * and {@link y}.
	 *
	 * Point is fundamental objects, most of the classes in this library
	 * depend on it, this class provide a some useful methods to help you slove
	 * problems when you deal with points.
	 *
	 * @since 0.1
	 * @see Vector
	 */
	[Version (since = "0.1")]
	public class Point : Object {
		/**
		 * x represent x-axis in coordinate system.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double x { get; set construct; }
		/**
		 * y represent y-axis in coordinate system.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double y { get; set construct; }

		/**
		 * Move this point to another location.
		 *
		 * {{{
		 * using Xyz;
		 *
		 * void main () {
		 *    var point = new Point (0.0, 0.0);
		 *    // x = 0 , y = 0
		 *    stdout.printf (@"$point\n");
		 *    // Output: ``(0, 0)``
		 *
		 *    point.move_to (15.0, 20.5);
		 *    // x = 15 , y = 20.5
		 *    stdout.printf (@"$point\n");
		 *    // Output: ``(15, 20.5)``
		 * }
		 * }}}
		 *
		 * @param x The new x-axis value
		 * @param y The new y-axis value
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void move_to (double x, double y) {
			this.x = x;
			this.y = y;
		}

		/**
		 * Transfer this point.
		 *
		 * {{{
		 * using Xyz;
		 *
		 * void main () {
		 *     var point = new Point (7.5, 4.0);
		 *     // x = 7.5 , y = 4
		 *     stdout.printf (@"$point\n");
		 *     // Output: ``(7.5, 4)``
		 *
		 *     point.transfer (new Vector (2.5, 6.0));
		 *     // x = 10 , y = 10
		 *     stdout.printf (@"$point\n");
		 *     // Output: ``(10, 10)``
		 * }
		 *
		 * }}}
		 *
		 * @param vector is the length and direction needed to transfer this point.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void transfer (Vector vector) {
			move_to (x + vector.x, y + vector.y);
		}

		/**
		 * Check whether this point and other are in the same location or not.
		 *
		 * @param other The other point to check it's location
		 * @return true if in the same location or false if not.
		 * @since 0.1
		 * @see near
		 */
		[Version (since = "0.1")]
		public bool equal (Point other) {
			return (x == other.x && y == other.y);
		}

		/**
		 * Check if this point is in the range with other point or not.
		 *
		 * @param other The other point to check it's distance with this point.
		 * @param range The the range that have to be checked.
		 * @return true if other point within the specified range with this point.
		 * @since 0.1
		 * @see equal
		 * @see distance
		 */
		[Version (since = "0.1")]
		public bool near (Point other, double range) {
			return (distance (other) <= range);
		}

		/**
		 * Return the centoid/midpoint between two points.
		 *
		 * @param other The other point
		 * @return The centroid point betwen this point and other.
		 * @since 0.1
		 * @see distance
		 */
		[Version (since = "0.1")]
		public Point centroid (Point other) {
			return new Point ((x + other.x) / 2.0, (y + other.y) / 2.0);
		}

		/**
		 * Return the slope point for two point.
		 *
		 * @param other The other point
		 * @return The slope of this
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double slope (Point other) {
			if ((other.y - y) == 0.0 && (other.x - x) == 0.0) {
				/* FIXME: this method may need to thorw exeption */
				return 0.0;
			}
			return ((other.y - y) / (other.x - x));
		}

		/**
		 * Return radian angle of this point(as center point) conneting to others.
		 *
		 * Examples: <<BR>>
		 * {{xyz_point_angle_figure_1.svg}}
		 * {{{
		 * using Xyz;
		 *
		 * void main () {
		 *     var point = new Point (-2.0, -2.0);
		 *     // x = -2 , y = -2
		 *
		 *     var rad_angle = point.angle (new Point (-2.0, 1.0), new Point (3.0, -2.0));
		 *     // a point is (-2, 1);   c point is (3, -2)
		 *
		 *     var deg_angle = to_degree (rad_angle);
		 *     stdout.printf (@"$deg_angle\n");
		 *     // Output: ``90``
		 * }
		 * }}}
		 *
		 * {{xyz_point_angle_figure_2.svg}}
		 * {{{
		 * using Xyz;
		 *
		 * void main () {
		 *     var point = new Point (-1.0, 2.0);
		 *     // x = -1 , y = 2
		 *
		 *     var rad_angle = point.angle (new Point (-5.0, 1.0), new Point (3.0, -1.0));
		 *     // a point is (-2, 1); c point is (3, -2)
		 *
		 *     var deg_angle = to_degree (rad_angle);
		 *     stdout.printf (@"$deg_angle\n");
		 *     // Output: ``230.9061411137705``
		 * }
		 * }}}
		 *
		 * @param a First point
		 * @param c Last point
		 * @return Angle in radian
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double angle (Point a, Point c) {
			// b is this point in this equation
			var ab_vec = new Vector.from_points (a, this);
			var cb_vec = new Vector.from_points (c, this);
			var dot = ab_vec.dot_product (cb_vec);
			var rad_angle = acos (dot / (ab_vec.length () * cb_vec.length ()));

			// Graham Scan
			var graham_scan = (x - a.x) * (c.y - a.y) - (y - a.y) * (c.x - a.x);
			if (graham_scan < 0) {
				rad_angle = (2 * PI) - rad_angle;
			}

			return rad_angle;
		}

		/**
		 * Return the the distance between two point
		 *
		 * @param other The other point.
		 * @return The distance.
		 * @since 0.1
		 * @see near
		 */
		[Version (since = "0.1")]
		public double distance (Point other) {
			return (sqrt (pow(x - other.x, 2.0) + pow(y - other.y, 2.0)));
		}

		/**
		 * Create a new {@link Xyz.Point} object.
		 *
		 * @param x Represent x-axis in coordinate system.
		 * @param y Represent y-axis in coordinate system.
		 * @since 0.1
		 * @see Point.from_point
		 * @see Point.from_vector
		 */
		[Version (since = "0.1")]
		public Point (double x = 0.0, double y = 0.0) {
			this.x = x;
			this.y = y;
		}

		/**
		 * Create a new {@link Xyz.Point} object from another point.
		 *
		 * This method will copy the other point values to this point.
		 *
		 * @param other The point that will be copied.
		 * @since 0.1
		 * @see Point
		 * @see Point.from_vector
		 */
		[Version (since = "0.1")]
		public Point.from_point (Point other) {
			this (other.x, other.y);
		}

		/**
		 * Create a new {@link Xyz.Point} object from vector.
		 *
		 * @param other The vector that will move the point.
		 * @since 0.1
		 * @see Point
		 * @see Point.from_point
		 */
		[Version (since = "0.1")]
		public Point.from_vector (Vector other) {
			this (other.x, other.y);
		}

		/**
		 * Return a string represent a point
		 *
		 * @return Point representation in string.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public string to_string () {
			return @"($x, $y)";
		}
	}
}
