/* xyz-line.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xyz {
	/**
	 * This class represent a Line segment in a 2D space and has two members
	 * {@link start} point and {@link end} point.
	 *
	 * @since 0.1
	 * @see Xyz.Curve
	 */
	[Version (since = "0.1")]
	public class Line : Object {
		/**
		 * Start point for this line.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point start { get; set construct; }
		/**
		 * End point for this line.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point end { get; set construct; }

		/**
		 * Move this line to a new postion.
		 *
		 * @param postion The new postion.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void move_to (Point postion) {
			var vector = new Vector.from_points (start, end);
			start.move_to (postion.x, postion.y);
			end.move_to (postion.x, postion.y);
			end.transfer (vector);
		}

		/**
		 * Transfer this line by adding a vector to its postion.
		 *
		 * Example:
		 * {{{
		 * using Xyz;
		 *
		 * void main () {
		 *     var line = new Line (5.0, 5.0, 10.0, 10.0);
		 *     var vector = new Vector (2.0, 5.0);
		 *
		 *     line.transfer (vector);
		 *     // line.start = (7, 10) , line.end = (12, 15)
		 * }
		 * }}}
		 *
		 * @param vector is the length and direction needed to transfer this line.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void transfer (Vector vector) {
			start.transfer (vector);
			end.transfer (vector);
		}

		/**
		 * Return the centoid/midpoint between first point and between second point.
		 *
		 * @return The centroid point.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point centroid () {
			return start.centroid (end);
		}

		/**
		 * Return the length for this line.
		 *
		 * @return length of this line.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double length () {
			return start.distance (end);
		}

		/**
		 * Return the slope for this line.
		 *
		 * @return slope of this line.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double slope () {
			return start.slope (end);
		}

		/**
		 * Check if this line parallel with other line.
		 *
		 * @param other The other line.
		 * @return true if parallel or false if not.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public bool is_parallel_with (Line other) {
			return (slope() == other.slope ());
		}

		/**
		 * Create a new {@link Xyz.Line} from the given parameters.
		 *
		 * @param start_x the x for {@link start} point in this object.
		 * @param start_y the y for {@link start} point in this object.
		 * @param end_x the x for {@link end} point in this object.
		 * @param end_y the y for {@link end} point in this object.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Line (double start_x = 0.0, double start_y = 0.0, double end_x = 1.0, double end_y = 1.0) {
			start = new Point (start_x, start_y);
			end = new Point (end_x, end_y);
		}

		/**
		 * Create a new {@link Xyz.Line} from two points.
		 *
		 * @param start this point will be copied to {@link start}.
		 * @param end this point will be copied to {@link end}.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Line.from_points (Point start, Point end) {
			this (start.x, start.y, end.x, end.y);
		}

		/**
		 * Create a new {@link Xyz.Line} from another line.
		 *
		 * This method will copy the other line values to this line.
		 *
		 * @param other The line that will be copied.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Line.form_line (Line other) {
			this.from_points (other.start, other.end);
		}

		/* TODO: add to_string method */
		/* TODO: add is_crosed_with_line method that retuen bool and one of it's parameter is out if true */
		/* TODO: add rotate (double angel, Point origin) method */
	}
}
